import { shallowMount } from '@vue/test-utils'
import Brand from '@/components/Brand.vue'

describe('Sortable bar chart', () => {
  it('renders bar chart', () => {
    const msg = 'new message'
    const wrapper = shallowMount(Brand)
    expect(wrapper.find("svg")).toBeTruthy()
  })
})
