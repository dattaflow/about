#!/bin/bash


IN=$(git tag --sort v:refname| tail -1 | cut -d"v" -f 2)
IFS='.' read -r -a array <<< "$IN"
MAJ=${array[0]} MIN=${array[1]} PAT=${array[2]}
case $1 in
    "major")i=$((MAJ++));;
    "minor")i=$((MIN++));;
    "patch")i=$((PAT++));;
    *) echo "incorrect arguments" break;;
esac
OUT="$MAJ"."$MIN"."$PAT"
npm version $OUT
git commit -m "new v$OUT"
git pull --rebase
